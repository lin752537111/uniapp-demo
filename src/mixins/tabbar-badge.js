import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters('cart', ['getTotal'])
  },
  watch: {
    getTotal () {
      this.setBadge()
    }
  },
  onShow () {
    this.setBadge()
  },
  methods: {
    setBadge () {
      if (this.getTotal) {
        uni.setTabBarBadge({
          index: 2,
          text: this.getTotal + ''
        })
      } else {
        uni.showTabBarRedDot({
          index: 2
        })
      }
    }
  }
}
