const BASEURL = 'https://www.uinav.com/api/public/v1/'
// 调用的时候传一个对象进来,这里就是做了对象的解构,
// 然后希望调用函数之后,通过then 或者async await拿到这结果
// 所以必须return一个 new Promise
const request = ({
  url,
  method = 'GET',
  data = {},
  header = {},
  tip = '拼命加载中...',
  isPullDownRefresh = false
}) => {
  return new Promise((resolve, reject) => {
    // 因为(8月7号,在subpkg\goods-list\index.vue添加了下拉刷新的逻辑)出现了一个bug
    // 所以在这里一个判断, 下面的关闭loging也要判断
    if (!isPullDownRefresh) {
      uni.showLoading({
        title: tip,
        mask: true
      })
    }
    // 8月11号添加
    const token = uni.getStorageSync('token')
    if (token) {
      header.Authorization = token
    }
    uni.request({
      // public/v1/最顶上的基础地址后面已经加了 / 所以这里就不需要加了
      url: `${BASEURL}${url}`,
      method,
      data,
      header,
      success: ({ data, statusCode, header }) => {
        // 成功后,返回数据
        resolve(data)
      },
      fail: error => {
        reject(error)
      },
      // 关闭loging效果
      complete: () => {
        if (!isPullDownRefresh) {
          uni.hideLoading()
        }
      }
    })
  })
}
// 挂载到全局的uni上去
// 注意封装好后 必须在main.js中导入这个文件  import './utils/request.js'
uni.$request = request
