export default {
  namespaced: true,
  state: {
    // 第五步,取值,取不到就给一个空
    goodsList: uni.getStorageSync('cart') || []
  },
  getters: {
    // 直接用goodsList也行,但是不建议直接用,(因为它是直接存储在仓库,直接用它的话会重新计算运行一些逻辑)
    // getCartList,用它就有缓存,这样性能会高一些,(放在缓存里面,就不需要重新计算,这样能提高性能)
    // 就可以直接从缓存里面读取数据啦
    getCartList (state) {
      return state.goodsList
    },
    // 第一步,8月9号添加
    // 购物车所有商品的总数量
    getTotal (state) {
      let total = 0
      state.goodsList.forEach(item => {
        total += item.goods_number
      })
      // 必须返回出去
      return total
    },
    // 获取选中的数量 (结算用)
    getSelectTotal (state) {
      let total = 0
      state.goodsList.forEach(item => {
        // goods_state是商品状态,如果为true,那就把它的数量加进来(components\my-settle\my-settle.vue页面中结算的件数用到了)
        if (item.goods_state) {
          total += item.goods_number
        }
      })
      // 必须返回出去
      return total
    },
    // 获取选中商品,的总价格 注意哦,需要 * 数量哦
    getSelectAmount (state) {
      let amount = 0
      state.goodsList.forEach(item => {
        if (item.goods_state) {
          amount += item.goods_number * item.goods_price
        }
      })
      // 必须返回出去
      return amount
    },
    // 给components\my-settle\my-settle.vue有页面的 radio全选按钮用的 :checked="isAllCheck"
    isAllCheck (state) {
      let isAllCheck = true
      // 遍历,然后判断其中一个没有选中,就设置为false
      state.goodsList.forEach(item => {
        if (!item.goods_state) {
          isAllCheck = false
        }
      })
      // 必须返回出去
      return isAllCheck
    }
  },
  mutations: {
    // src\subpkg\detail\index.vue页面传递过来的数据,那边使用了赋值函数传值this.addToCart(goods)
    addToCart (state, goods) {
      console.log('goods', goods)
      // find()方法用于找出第一个符合条件的数组成员,元素
      const findGoods = state.goodsList.find(
        item => item.goods_id === goods.goods_id
      )
      // 如果购物车中之前存在,则把数量累加
      if (findGoods) {
        // console.log('lin')
        findGoods.goods_number += goods.goods_number
        // console.log('ll2')
      } else {
        state.goodsList.push(goods)
      }
      // 把数据保存到本地, 不保存的话,刷新后数据就没有了
      // 将 data 存储在本地缓存中指定的 key 中，会覆盖掉原来该 key 对应的内容，这是一个同步接口。
      uni.setStorageSync('cart', state.goodsList)
    },
    // 更改商品状态
    updateGoodsState (state, goods) {
      const findGoods = state.goodsList.find(
        item => item.goods_id === goods.goods_id
      )
      if (findGoods) {
        // 注意,这里不能是全等,因为是赋值
        // 传递进来的状态赋值给仓库中原本的状态
        findGoods.goods_state = goods.goods_state
      }
      // 把数据保存到本地
      uni.setStorageSync('cart', state.goodsList)
    },
    // 更改商品数量
    updateGoodsNumber (state, goods) {
      const findNumber = state.goodsList.find(
        item => item.goods_id === goods.goods_id
      )
      if (findNumber) {
        // 注意,这里不能是全等,因为是赋值
        // 传递进来的goods_number赋值给仓库中的goods_number
        findNumber.goods_number = goods.goods_number
      }
      // 把数据保存到本地
      uni.setStorageSync('cart', state.goodsList)
    },
    // 根据id删除
    deleteGoodsById (state, goods_id) {
      // 方法1
      const findIndex = state.goodsList.findIndex(
        item => item.goods_id === goods_id
      )
      // 上面的等等于说明找到了, findIndex方法如果没有找到就返回-1
      // 所以,如果这个值大于-1说明找到了
      if (findIndex > -1) {
        // 然后删除
        state.goodsList.splice(findIndex, 1)
      }
      // 方法2
      // state.goodsList = state.goodsList.filter(
      // 不等于传递过来的goods_id,也就是说把传递过来id过滤掉了,就不要了
      //   item => item.goods_id !== goods_id
      // )
      // 把数据保存到本地
      uni.setStorageSync('cart', state.goodsList)
    },
    // 全选和反选
    updateAllGoodsState (state, goods_state) {
      state.goodsList.forEach(item => {
        //  goods_state 传递过来的状态
        item.goods_state = goods_state
      })
      // 把数据保存到本地
      uni.setStorageSync('cart', state.goodsList)
    },
    setUserInfo (state, userInfo) {
      state.userInfo = userInfo

      uni.setStorageSync('userInfo', state.userInfo)
    },
    setToken (state, token) {
      state.token = token

      uni.setStorageSync('token', state.token)
    }
  }
}
