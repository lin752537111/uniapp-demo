export default {
  namespaced: true,
  state: {
    //   getStorageSync,从本地缓存中同步获取指定 key 对应的内容。
    address: uni.getStorageSync('address'),
    token: uni.getStorageSync('token'),
    userInfo: uni.getStorageSync('userInfo'),

    navUrl: null
  },
  getters: {
    //   获取用户数据的
    getAddress (state) {
      return state.address
    },
    // 拼接的收货地址,为了布局不要写台长的代码
    addressStr (state) {
      if (state.address) {
        return `${state.address.provinceName}${state.address.cityName}${state.address.countyName}${state.address.detailInfo}`
      }
    },
    getToken (state) {
      return state.token
    },
    // 获取用户信息
    getUserInfo (state) {
      return state.userInfo
    },
    getNavUrl (state) {
      return state.navUrl
    }
  },
  mutations: {
    //   保存用户的收货地址
    setAddress (state, address) {
      state.address = address
      //   把数据保存到本地
      uni.setStorageSync('address', address)
    },
    // 保存跳转时的地址
    setNavUrl (state, navUrl) {
      state.navUrl = navUrl
    },
    // 保存用户token
    setToken (state, token) {
      state.token = token
      uni.setStorageSync('token', state.token)
    },
    // 保存用户信息
    setUserInfo (state, userInfo) {
      state.userInfo = userInfo
      uni.setStorageSync('userInfo', state.userInfo)
    },
  }
}
